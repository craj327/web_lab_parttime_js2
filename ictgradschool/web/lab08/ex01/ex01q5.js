"use strict";

// Provided variable
var theString = "Hello World!";

// Variable you'll be modifying
var reversed = "";

// TODO Your answer here.


for (var i = theString.length; i > 0; i--){

    reversed = reversed + theString.charAt(i-1);
}

// Printing the answer.
console.log("\"" + theString + "\", reversed, is: \"" + reversed + "\".");

// var numbers = [-9, 2, 7, 5, 124, -5, 1, 144];