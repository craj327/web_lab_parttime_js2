"use strict";

// FUNCTIONS
// ------------------------------------------

// TODO Complete this function, which should generate and return a random number between a given lower and upper bound (inclusive)
function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
//console.log(getRndInteger(20, 80));


// TODO Complete this function, which should round the given number to 2dp and return the result.
function roundTo2dp(number) {
    var randomNumber = Math.round(number * 100) / 100; 
    return randomNumber;
}

//console.log(roundTo2dp(2.9896795));

// TODO Write a function which calculates and returns the volume of a cone.

function volumeOfCone(radius, height){
    var result = (Math.PI * (radius*radius) * height) / 3;
    return result; 
}
//console.log(volumeOfCone(10, 30));

// TODO Write a function which calculates and returns the volume of a cylinder.

function VolumeOfCylinder(radius, height){
    var result = (Math.PI * (radius * radius) * height)
    return result;
}

//console.log(VolumeOfCylinder(20, 60));

// TODO Write a function which prints the name and volume of a shape, to 2dp.

function ShapeAndVolume(volume, shape){
    var result = "The Volume of the " + shape + " is: " + roundTo2dp(volume) + " cm^3";
    return result;
} 
//console.log (ShapeAndVolume(23, "Cone"));

// ------------------------------------------

// TODO Complete the program as detailed in the handout. You must use all the functions you've written appropriately.

function x() {

var coneRadius = getRndInteger(25, 50);
var cylinderRadius = getRndInteger(25, 50);
var coneHeight = getRndInteger(25, 50);
var cylinderHeight = getRndInteger(25, 50);

var calVolCone = volumeOfCone(coneRadius, coneHeight);
var calVolCylinder = VolumeOfCylinder(cylinderRadius, cylinderHeight);

var largOfTheVol = Math.max(calVolCone, calVolCylinder);

var largOfTheShape = '';

if(calVolCone == largOfTheVol)
{
    largOfTheShape = 'Cone';
}
else
{
    largOfTheShape = 'Cylinder';
}

var result = "The Volume of the cone is: " + Math.round(calVolCone * 100) /100  + " cm^3 \n" + 
             "The Volume of the cylinder is: " + Math.round(calVolCylinder * 100) /100 + " cm^3" + "\n" +   
             "The shape with the largest volume is the " + Math.round(largOfTheVol * 100) /100 + ", with a volume of " + largOfTheShape;  

    return result;
}

console.log(x());
